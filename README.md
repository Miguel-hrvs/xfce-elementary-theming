Things that I use to theme my xfce wrapped here.

1. Install fish and fastfetch from your package manager or build them.

2. Make fish as your default shell with chsh -s $(which fish).

3. Copy the files from this repo.

4. Clear the system font cache using the fc-cache

Notes: 
    
    If you are new to xfce and see screen tearing go to the following pages:
        - For intel gpu: https://wiki.archlinux.org/title/Intel_graphics or https://wiki.gentoo.org/wiki/Intel
        - For amd gpu: https://wiki.archlinux.org/title/AMDGPU or https://wiki.gentoo.org/wiki/AMDGPU
        - For ati gpu: https://wiki.archlinux.org/title/ATI or https://wiki.gentoo.org/wiki/Radeon

    To make fastfetch logos from an image use this: https://github.com/TheZoraiz/ascii-image-converter
        - Use the -b or -b --dither options to make similar style ascii art.

    If you want to use the content with the root user and lightdm, you'll have to move the archives to their corresponding root folder: /usr/share/icons /usr/share/themes /usr/share/fonts

    Original fastfetch logo source: https://github.com/Chick2D/neofetch-themes/blob/main/normal/bejkon2/logo 
    
    Original icons repo: https://github.com/shimmerproject/elementary-xfce
    
    Original emojis tutorial: https://virola.io/articles/how-to-get-emojis-working-on-linux#:~:text=We%20recommend%20using%20Noto%20Color,in%20TrueType%20(TTF)%20format
    
    Recommended repo for grub themes: https://github.com/Coopydood/HyperFluent-GRUB-Theme/tree/main
        - If you have a lot of grub entries make them tinier to fit changing this on theme.txt:
            
            icon_width = 25
            icon_height = 25
            item_icon_space = 10
            item_height = 40
            item_padding = 3
            item_spacing = 10

    If the cursor icon theme doesn't show up use this alternative: https://github.com/kaesetoast/elementary-cursors
